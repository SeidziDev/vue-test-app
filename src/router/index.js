import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/login',
      name: 'login',
      component: () => import('@/components/auth/login')
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: () => import('@/components/dashboard/dashboard'),
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '*',
      redirect: '/dashboard'
    }
  ]
})
router.beforeEach((to, from, next) => {
  if (to.meta.requiresAuth) {
    const authUser = JSON.parse(localStorage.getItem('accessToken'));
    if (authUser && authUser.accessToken) {
      next();
    }
    else {
      next({
        path: '/login'
      })
    }
  }
  else {
    next();
  }
});

export default router